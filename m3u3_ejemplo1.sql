﻿DROP DATABASE IF EXISTS Ejemplo1Programacion1;
CREATE DATABASE Ejemplo1Programacion1;
USE Ejemplo1Programacion1;

                                                    /* Ejemplo 1 */

/* 1. Realizar un procedimiento almacenado que reciba DOS numeros y te indique el mayor de ellos (realizarle con instruccion if,
  con consulta de totales y con una funcion de Mysql */
 -- a) con instruccion if
 
  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_1a //

  CREATE PROCEDURE ejem1_1a(num1 int, num2 int)
  BEGIN
    DECLARE s int DEFAULT num2;
    -- DECLARE s int DEFAULT 0;

    IF (num1>num2) THEN SET s=num1;
      -- ELSE SET s=num2;
    END IF;

    SELECT s;
  END //
    DELIMITER;

CALL ejem1_1a(3,4);
CALL ejem1_1a(7,6);

-- b) con consulta de totales

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_1b //

  CREATE PROCEDURE ejem1_1b(num1 int, num2 int)
  BEGIN
    DROP TABLE IF EXISTS tabla1_1b;
    CREATE TEMPORARY TABLE tabla1_1b(
    id int AUTO_INCREMENT PRIMARY KEY,
    numero1 int
    );
    INSERT INTO tabla1_1b VALUES
    (DEFAULT,num1),
    (DEFAULT, num2);
    SELECT MAX(numero1) FROM tabla1_1b;
  END //
  DELIMITER;

CALL ejem1_1b(3,4);
CALL ejem1_1b(7,6);

-- c) con funcion MySQL

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_1c //

  CREATE PROCEDURE ejem1_1c(num1 int, num2 int)
  BEGIN
    SELECT GREATEST(num1,num2);
  END //
  DELIMITER;

CALL ejem1_1c(3,4);
CALL ejem1_1c(7,6);


/* 2. Realizar un procedimiento almacenado que reciba TRES numeros y te indique el mayor de ellos (realizarle con instruccion if,
  con consulta de totales y con una funcion de Mysql */

 -- a) con instruccion if
 
  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_2a //

  CREATE PROCEDURE ejem1_2a(num1 int, num2 int, num3 int)
  BEGIN
    DECLARE s int DEFAULT num3;      -- la que mas se repita en el organigrama
    IF (num1>num2) 
      THEN 
      IF (num1>num3) 
        THEN 
        SET s=num1;
      END IF;
      ELSEIF (num2>num3) 
      THEN
      set s=num2;
    END IF;
    
    SELECT s;

  END //
    DELIMITER;

CALL ejem1_2a(3,4,8);
CALL ejem1_2a(7,6,2);

-- b) con consulta de totales

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_2b //

  CREATE PROCEDURE ejem1_2b(num1 int, num2 int)
  BEGIN
    DROP TABLE IF EXISTS tabla1_2b;
    CREATE TEMPORARY TABLE tabla1_2b(
    id int AUTO_INCREMENT PRIMARY KEY,
    numero1 int
    );
    INSERT INTO tabla1_2b VALUES
    (DEFAULT,num1),
    (DEFAULT,num2),
    (DEFAULT,num3);

    SELECT MAX(numero1) FROM tabla1_2b;
  END //
  DELIMITER;

CALL ejem1_2b(3,4,8);
CALL ejem1_2b(7,6,2);

-- c) con funcion MySQL

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_2c //

  CREATE PROCEDURE ejem1_2c(num1 int, num2 int,num3 int)
  BEGIN
    SELECT GREATEST(num1,num2,num3);
  END //
  DELIMITER;

CALL ejem1_2c(3,4,8);
CALL ejem1_2c(7,6,2);


/* 3. Realizar un procedimiento almacenado que reciba tres numeros
  y dos argumentos de tipo salida donde devuelva el numero mas grande
  y el numero mas pequeño de los tres numeros pasados */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_3 //

  CREATE PROCEDURE ejem1_3(arg1 int, arg2 int, arg3 int, OUT mayor int, OUT menor int)
  BEGIN
      SET mayor=GREATEST(arg1,arg2,arg3);
      SET menor=LEAST(arg1,arg2,arg3);

  END //
  DELIMITER;

  -- **OJO!! PECULIARIDAD: hacemos el SELECT fuera del procedure**
CALL ejem1_3(5,7,9,@mayor,@menor);
SELECT @mayor,@menor;

/* 4. Realizar un procedimiento almacenado que reciba dos fechas y te muestre
  el numero de dias de diferencia entre las dos fechas */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_4 //

  CREATE PROCEDURE ejem1_4(fecha1 date, fecha2 date)
  BEGIN

    SELECT DATEDIFF(fecha1,fecha2) dias;

  END //
  DELIMITER;

CALL ej4('2017-10-25','2020-12-1');

/* 5. Realizar un procedimiento almacenado que reciba dos fechas y te muestre
  el numero de meses de diferencia entre las dos fechas */

DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_5 //

  CREATE PROCEDURE ejem1_5(fecha1 date, fecha2 date)
  BEGIN
        SELECT TIMESTAMPDIFF(MONTH,fecha1,fecha2);

  END //
  DELIMITER;

CALL ej5('2017/10/25','2020/12/1');

/* 6. Realizar un procedimiento almacenado que reciba dos fechas y te devuelva
  en 3 argumentos de salida los dias, meses y años entre las dos fechas */

DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_6 //

  CREATE PROCEDURE ejem1_6(fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT annos int)
  BEGIN
      SET dias=TIMESTAMPDIFF(DAY,fecha1,fecha2);
      SET meses=TIMESTAMPDIFF(MONTH,fecha1,fecha2);
      SET annos=TIMESTAMPDIFF(YEAR,fecha1,fecha2);

  END //
  DELIMITER;

CALL ej6('2017-10-25','2020-12-1',@d,@m,@y);
SELECT @d,@m,@y;



/* 7. Realizar un procedimiento almacenado que reciba una frase y te muestre el numero de caracteres */

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_1c //

  CREATE PROCEDURE ejem1_1c(frase varchar(150))
  BEGIN
      SELECT LENGTH(frase);

  END //
  DELIMITER;

CALL ej7('frase de prueba');